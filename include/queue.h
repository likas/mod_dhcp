#ifndef MAIN_HEADER_H
#define MAIN_HEADER_H

#include <pthread.h>
#include <semaphore.h>
#include <error.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

#include <time.h>
#include <sys/time.h>

#include <string.h>

enum ERRORS{ QFULL = 10 };

struct queue{
    sem_t crit_sec;
    sem_t is_message;
    void **buffer; //pointer to array of pointers
    int head;
    int tail;  
    size_t length;
};

int init_queue(struct queue*, size_t);
int close_queue(struct queue*);
int put_queue(struct queue*, void*, int);
int get_queue(struct queue*, void*, int);

#endif
