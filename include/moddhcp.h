#ifndef MODDHCP_H
#define MODDHCP_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
/* !! */
//#include <netdb.h>

//#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>

#include <net/if.h>
#include <net/if_arp.h>
#include <net/ethernet.h>
#include <netpacket/packet.h>

/* ** */
#include <pthread.h>

#include <string.h>
#include <signal.h>

#include "queue.h"

/* globals */
int socksrv_in,
    socksrv_out;
struct sockaddr_in serv_addr;
struct sockaddr_in s_addr_out;
struct queue *serv_send;
int quit;

#define HARDWARE_ADDR_LEN 16

#define DHCP_UDP_OVERHEAD	(20 + /* IP header */			\
			                8)   /* UDP header */
#define DHCP_SNAME_LEN		64
#define DHCP_FILE_LEN		128
#define DHCP_FIXED_NON_UDP	236
#define DHCP_FIXED_LEN		(DHCP_FIXED_NON_UDP + DHCP_UDP_OVERHEAD)
						/* Everything but options. */
#define BOOTP_MIN_LEN		300

#define DHCP_MTU_MAX		1500
#define DHCP_MTU_MIN        576

#define DHCP_MAX_OPTION_LEN	(DHCP_MTU_MAX - DHCP_FIXED_LEN)
#define DHCP_MIN_OPTION_LEN     (DHCP_MTU_MIN - DHCP_FIXED_LEN)
#define DHCP_CLIENT_ID_LEN 32

#define IFACE_STRLEN 16

struct dhcp_packet{
    u_int8_t  op;		/* 0: Message opcode/type */
	u_int8_t  htype;	/* 1: Hardware addr type (net/if_types.h) */
	u_int8_t  hlen;		/* 2: Hardware addr length */
	u_int8_t  hops;		/* 3: Number of relay agent hops from client */
	u_int32_t xid;		/* 4: Transaction ID */
	u_int16_t secs;		/* 8: Seconds since client started looking */
	u_int16_t flags;	/* 10: Flag bits */
	struct in_addr ciaddr;	/* 12: Client IP address (if already in use) */
	struct in_addr yiaddr;	/* 16: Client IP address */
	struct in_addr siaddr;	/* 18: IP address of next server to talk to */
	struct in_addr giaddr;	/* 20: DHCP relay agent IP address */
	unsigned char chaddr [HARDWARE_ADDR_LEN];	/* 24: Client hardware address */
	char sname [DHCP_SNAME_LEN];	/* 40: Server name */
	char file [DHCP_FILE_LEN];	/* 104: Boot filename */
	unsigned char options [DHCP_MAX_OPTION_LEN];
				/* 212: Optional parameters
			  (actual length dependent on MTU). */
};

struct hardware{
    u_int8_t hlen;
    u_int8_t hbuf[HARDWARE_ADDR_LEN + 1];
};

struct lease{
    struct lease *next;
    u_int32_t xid;
    struct in_addr ip;
    u_int32_t time;
    struct hardware h_addr;
} *f_lease;

struct o_packet{
    struct dhcp_packet *raw;
    int type;
    int options_length;
    int options_valid;
    struct in_addr req_addr;
    struct in_addr srv_addr;
    unsigned char client_id[DHCP_CLIENT_ID_LEN];
}; 

struct s_state{
    int state;
    struct lease *c_lease;
    int lease_status;
};

/* BOOTP (rfc951) message types */
#define	BOOTREQUEST	1
#define BOOTREPLY	2

/* Magic cookie validating dhcp options field (and bootp vendor
   extensions field). */
#define DHCP_OPTIONS_COOKIE	"\143\202\123\143"

/* Possible values for flags field... */
#define BOOTP_BROADCAST 32768L

/* Possible values for hardware type (htype) field...           */
#define HTYPE_ETHER	      1         /* Ethernet 10Mbps          */
#define HTYPE_IEEE802     6         /* IEEE 802.2 Token Ring...	*/
#define HTYPE_FDDI	      8	    	/* FDDI...	        		*/
#define HTYPE_INFINIBAND  32		/* IP over Infiniband		*/
#define HTYPE_IPMP       255            /* IPMP - random hw address - there
					 * is no standard for this so we
					 * just steal a type            */

/* DHCP message types. */
#define DHCPDISCOVER	    1
#define DHCPOFFER		    2
#define DHCPREQUEST		    3
#define DHCPDECLINE		    4
#define DHCPACK			    5
#define DHCPNAK			    6
#define DHCPRELEASE		    7
#define DHCPINFORM		    8
#define DHCPLEASEQUERY		10
#define DHCPLEASEUNASSIGNED	11
#define DHCPLEASEUNKNOWN	12
#define DHCPLEASEACTIVE		13


enum err{ PARAMS_INCORRECT = 10 };
enum types{ UCHAR = 1, LONGINT };
enum states{ START = 0, REQUEST_WAIT, RENEW_WAIT, CLOSE };
enum lease_states{ NONE, TEMP, ACTIVE };

int parse_options(struct o_packet* packet);
int check_and_parse(struct o_packet* packet);
int print_packet(struct o_packet*);
void* thread_service(void*);
void* thread_sender(void*);
void int_handler(int);
int dhcpdiscover(struct o_packet *packet, struct in_addr ip_lease);
int dhcprequest(struct o_packet *packet, u_int32_t time, \
        struct in_addr ip_lease);
int fill_option(struct dhcp_packet* packet, unsigned char option, \
          int option_type, unsigned char length, int position, ...);
int get_option(struct dhcp_packet* packet, unsigned char option, \
        int length, int option_type, void* destination);
struct lease find_lease(struct o_packet* packet);
int generate_ip(struct in_addr *tmp_inaddr);
int step(struct s_state* now, struct o_packet* packet);
int fill_packet(struct dhcp_packet* packet, int packet_type);
struct lease* check_lease_avail(u_int32_t ip);
int set_connection( char* interface, struct sockaddr_in* addr_out);
int pid_action(char* argu);
int get_raw_sock();
int get_dgram_sock();
int proceed_packet(struct dhcp_packet* packet, unsigned char* buffer, int size);
void print_data(unsigned char* data, int size);
int fill_frame(uint8_t* eth_frame, int sock, char* mac, int mac_len, struct o_packet* data, int datalen);
int send_cooked(int sock, struct dhcp_packet* packet, const char* iface);
int send_raw(int sock, struct dhcp_packet* packet, const char* iface);
int print_hwaddr(unsigned char*, int);

/* DHCP Option codes: */

#define DHO_PAD					            0
#define DHO_SUBNET_MASK				        1
#define DHO_TIME_OFFSET				        2
#define DHO_ROUTERS				            3
#define DHO_TIME_SERVERS			        4
#define DHO_NAME_SERVERS			        5
#define DHO_DOMAIN_NAME_SERVERS		        6
#define DHO_LOG_SERVERS				        7
#define DHO_COOKIE_SERVERS			        8
#define DHO_LPR_SERVERS				        9
#define DHO_IMPRESS_SERVERS			        10
#define DHO_RESOURCE_LOCATION_SERVERS	    11
#define DHO_HOST_NAME				        12
#define DHO_BOOT_SIZE				        13
#define DHO_MERIT_DUMP				        14
#define DHO_DOMAIN_NAME				        15
#define DHO_SWAP_SERVER				        16
#define DHO_ROOT_PATH				        17
#define DHO_EXTENSIONS_PATH			        18
#define DHO_IP_FORWARDING			        19
#define DHO_NON_LOCAL_SOURCE_ROUTING	    20
#define DHO_POLICY_FILTER			        21
#define DHO_MAX_DGRAM_REASSEMBLY		    22
#define DHO_DEFAULT_IP_TTL			        23
#define DHO_PATH_MTU_AGING_TIMEOUT		    24
#define DHO_PATH_MTU_PLATEAU_TABLE		    25
#define DHO_INTERFACE_MTU			        26
#define DHO_ALL_SUBNETS_LOCAL			    27
#define DHO_BROADCAST_ADDRESS			    28
#define DHO_PERFORM_MASK_DISCOVERY		    29
#define DHO_MASK_SUPPLIER			        30
#define DHO_ROUTER_DISCOVERY		    	31
#define DHO_ROUTER_SOLICITATION_ADDRESS	    32
#define DHO_STATIC_ROUTES			        33
#define DHO_TRAILER_ENCAPSULATION	    	34
#define DHO_ARP_CACHE_TIMEOUT		    	35
#define DHO_IEEE802_3_ENCAPSULATION		    36
#define DHO_DEFAULT_TCP_TTL			        37
#define DHO_TCP_KEEPALIVE_INTERVAL		    38
#define DHO_TCP_KEEPALIVE_GARBAGE		    39
#define DHO_NIS_DOMAIN				        40
#define DHO_NIS_SERVERS				        41
#define DHO_NTP_SERVERS				        42
#define DHO_VENDOR_ENCAPSULATED_OPTIONS	    43
#define DHO_NETBIOS_NAME_SERVERS		    44
#define DHO_NETBIOS_DGRAM_DISTRIBUTION      45
#define DHO_NETBIOS_NODE_TYPE			    46
#define DHO_NETBIOS_SCOPE			        47
#define DHO_FONT_SERVERS			        48
#define DHO_X_DISPLAY_MANAGER		    	49
#define DHO_DHCP_REQUESTED_ADDRESS	    	50
#define DHO_DHCP_LEASE_TIME			        51
#define DHO_DHCP_OPTION_OVERLOAD	    	52
#define DHO_DHCP_MESSAGE_TYPE		    	53
#define DHO_DHCP_SERVER_IDENTIFIER		    54
#define DHO_DHCP_PARAMETER_REQUEST_LIST	    55
#define DHO_DHCP_MESSAGE			        56
#define DHO_DHCP_MAX_MESSAGE_SIZE		    57
#define DHO_DHCP_RENEWAL_TIME			    58
#define DHO_DHCP_REBINDING_TIME			    59
#define DHO_VENDOR_CLASS_IDENTIFIER		    60
#define DHO_DHCP_CLIENT_IDENTIFIER		    61
#define DHO_NWIP_DOMAIN_NAME			    62
#define DHO_NWIP_SUBOPTIONS			        63
#define DHO_USER_CLASS				        77
#define DHO_FQDN				            81
#define DHO_DHCP_AGENT_OPTIONS			    82
#define DHO_AUTHENTICATE			        90  /* RFC3118, was 210 */
#define DHO_CLIENT_LAST_TRANSACTION_TIME    91
#define DHO_ASSOCIATED_IP			        92
#define DHO_SUBNET_SELECTION			    118 /* RFC3011! */
#define DHO_DOMAIN_SEARCH			        119 /* RFC3397 */
#define DHO_VIVCO_SUBOPTIONS			    124
#define DHO_VIVSO_SUBOPTIONS			    125

#define DHO_END					            255

#define DEFAULT_DEFAULT_LEASE_TIME 43200
#define DEFAULT_MIN_LEASE_TIME 300
#define DEFAULT_MAX_LEASE_TIME 86400

#endif /* MODDHCP_H */
