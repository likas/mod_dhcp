#include "../include/moddhcp.h"

struct lease* check_lease_avail(u_int32_t ip){
    struct lease *lease_pointer;
    lease_pointer = f_lease;
    
            for( ; lease_pointer -> next != NULL ; ){
                 if( lease_pointer -> ip.s_addr != ip ){
                     lease_pointer = lease_pointer -> next;
                 }else{
                    return NULL;
                 }
            }
    return lease_pointer;
}
