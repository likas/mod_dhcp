#include "../include/moddhcp.h"

int set_connection(char* interface, struct sockaddr_in* addr_out){
    int sock_in;
    int sso_ret;
    int bind_ret;
    int broadcast_flag;
    broadcast_flag = 1;
	struct sockaddr_ll addr_in;
    if ((sock_in = socket(AF_PACKET, SOCK_RAW, IPPROTO_UDP)) == -1) {
        perror("socket");
        return -1;
    }

    struct ifreq req;
    strncpy(req.ifr_name, interface, IFNAMSIZ-1);
    if (ioctl(sock_in, SIOCGIFINDEX, &req) < 0){
        perror("ioctl:SIOCGIFINDEX");
        return -1;
    }

	//addr_in.sin_family = AF_INET;
    //addr_in.sin_addr.s_addr = INADDR_ANY;
	//addr_in.sin_port=htons(68); 
    addr_in.sll_family = AF_PACKET;
    addr_in.sll_ifindex = req.ifr_ifindex;
    addr_in.sll_protocol = IPPROTO_UDP;

    sso_ret = setsockopt(sock_in, SOL_SOCKET, SO_BROADCAST, \
            &broadcast_flag, \
            sizeof(int)); 
    if( sso_ret == -1 ){
        perror("setsockopt");
        return EXIT_FAILURE;
    } 

    /* binding */
    bind_ret = bind(sock_in, (struct sockaddr*)&addr_in, \
        sizeof(struct sockaddr_ll)); 
    if( bind_ret == -1 ){
        perror("bind");
        return EXIT_FAILURE;
    } 

	addr_out -> sin_family = AF_INET;
    addr_out -> sin_addr.s_addr = htonl(INADDR_BROADCAST);
	addr_out -> sin_port=htons(67);

    return sock_in;
}
