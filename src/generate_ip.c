#include "../include/moddhcp.h"

int generate_ip(struct in_addr *tmp_inaddr){
    srand(time(NULL));
    unsigned char ip[4];
    ip[0] = 192; ip[1] = 168; ip[2] = 0;
    ip[3] = 1 + rand()%253;
    memcpy(&(tmp_inaddr -> s_addr), ip, 4);
    return EXIT_SUCCESS;
}
