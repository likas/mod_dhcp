#include "../include/moddhcp.h"

static const char *dhcp_type_names [] = { 
    "DHCPDISCOVER",
	"DHCPOFFER",
	"DHCPREQUEST",
	"DHCPDECLINE",
	"DHCPACK",
	"DHCPNAK",
	"DHCPRELEASE",
	"DHCPINFORM",
	"type 9",
	"DHCPLEASEQUERY",
	"DHCPLEASEUNASSIGNED",
	"DHCPLEASEUNKNOWN",
	"DHCPLEASEACTIVE"
};

int print_packet(struct o_packet* packet){
    printf("in print_packet:\n");
    printf("op: \t%d\n", packet -> raw -> op );
    printf("htype: \t%d\n",  packet -> raw -> htype );
    printf("hlen:\t%d\n", packet -> raw -> hlen);
    printf("hops:\t%d\n", packet -> raw -> hops);
    printf("xid:\t%x\n", ntohl(packet -> raw -> xid));
    printf("secs:\t%d\n", (int) packet -> raw -> secs);
    printf("flags:\t%x\n", packet -> raw -> flags);
    printf("ciaddr:\t%s\n", (inet_ntoa( packet -> raw -> ciaddr )) );
    printf("yiaddr:\t%s\n", (inet_ntoa( packet -> raw -> yiaddr )) );
    printf("siaddr:\t%s\n", (inet_ntoa( packet -> raw -> siaddr )) );
    printf("giaddr:\t%s\n", (inet_ntoa( packet -> raw -> giaddr )) );
    print_hwaddr( packet -> raw -> chaddr, packet -> raw -> hlen);
    
    printf(" ---- options ----- \n");

    printf("type:\t%s\n", dhcp_type_names[packet -> type - 1]);
    printf("req addr:\t%s\n", (inet_ntoa( packet -> req_addr )) );
    printf("srv addr:\t%s\n", (inet_ntoa( packet -> srv_addr )) );
    printf("\n\n"); 




    return EXIT_SUCCESS;
}
