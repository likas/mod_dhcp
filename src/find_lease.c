#include "../include/moddhcp.h"

struct lease find_lease(struct o_packet* packet){
    struct lease tmp_lease;

    /* if client didnt ask for specific ip */
    if( packet -> req_addr.s_addr == 0 ){
        generate_ip(&(tmp_lease.ip));
    }else{
        /* if he does */
        memcpy(&(tmp_lease.ip.s_addr), \
        &(packet -> req_addr.s_addr), 4);
    }
        /* if there's leases already */
        if( f_lease != NULL ){
            printf("generated: %s\n", inet_ntoa( tmp_lease.ip ));
            for( ; check_lease_avail(tmp_lease.ip.s_addr); \
                    generate_ip(&(tmp_lease.ip)) ){}
//            lease_pointer -> next = malloc(sizeof(struct lease));
        }else{
            /* if not */
            f_lease = malloc(sizeof(struct lease));
            generate_ip(&(tmp_lease.ip));
            printf("generated: %s\n", inet_ntoa( tmp_lease.ip ));
        }

    /* filling part */
    /* fill xid */
    tmp_lease.xid = packet -> raw -> xid;

    /* fill time */
    tmp_lease.time = DEFAULT_DEFAULT_LEASE_TIME;
    /* fill hw address */
    tmp_lease.h_addr.hlen = packet -> raw -> hlen;
    strncpy( tmp_lease.h_addr.hbuf, packet -> raw -> chaddr, \
            packet -> raw -> hlen );
    return tmp_lease;
}
