#include "../include/moddhcp.h"

#define N 16

int main(int argc, char* argv[]){
    int i;
    int pp_ret;
    int sock;
    int addr_len = sizeof( struct sockaddr_in );
/*    if( argc > 2 ){
        pid_action(argv[2]);
    }else{
        pid_action(NULL);
    } */
    quit = 1;
    /* ------------------------- locals ------------------------------*/
    int actual_length;
    unsigned char buffer[65536];
    /* + memory for packet + */
    struct o_packet* packet = malloc(sizeof(struct o_packet));
    packet -> raw = malloc(sizeof(struct dhcp_packet));
    /* = queues and xids = */
    struct queue* queue_array[N];
    u_int32_t xid[N];
    for( i = 0; i < N; ++i){ xid[i] = -1; }
    pthread_t t[N];
    serv_send = malloc(sizeof (struct queue));
    init_queue(serv_send, 16);
    /* ---------------------- network setting ------------------------*/
    sock = get_dgram_sock();

    serv_addr.sin_addr.s_addr = inet_addr("192.168.0.100");
    //getsockname(sock, (struct sockaddr*)&serv_addr, \
            (socklen_t*) &addr_len);

    /* ---------------------- creating a thread --------------------- */
    pthread_t thread_send;
    pthread_create(&thread_send, NULL, (void*)thread_sender, \
            NULL); 
    /* ----------------------- set signal action -------------------- */
    struct sigaction act;
    act.sa_handler = int_handler;
    sigaction(SIGINT, &act, NULL);
    /* ------------------------ main, finally ------------------------*/
    while( quit ){
        actual_length = recvfrom(sock, (void*)buffer, 65536, 0, NULL, 0);
        if( actual_length == -1 ){
            perror("recvfrom");
            quit = 0;
            break;
        }

        pp_ret = proceed_packet(packet -> raw, buffer, actual_length);
        if( pp_ret ){
            continue;
        }

        for( i = 0; packet -> raw -> xid != xid[i] && i < N && xid[i] != -1; ++i ){}
        /* if new xid */
        if( xid[i] == -1 ){
            xid[i] = packet -> raw -> xid;
            queue_array[i] = malloc(sizeof(struct queue));
            init_queue(queue_array[i], 8);
            pthread_create(&t[i], NULL, (void*)thread_service, \
                     queue_array[i]);
            printf("Thread_created [%x]\n", (int)t[i]);
            put_queue(queue_array[i], packet, sizeof(struct o_packet));
        }else{
            /* if not new */
            put_queue(queue_array[i], packet, sizeof(struct o_packet));
        }
 
    }
    /* ----------------------- closin' all ---------------------------*/
    /* TODO pthread_join */
     for( i = 0; xid[i] != -1; ++i ){
        close_queue(queue_array[i]);
    }

    close(socksrv_in);
    close(socksrv_out);
    close_queue(serv_send);
    free(serv_send);
    free(packet);
    return EXIT_SUCCESS;
}
