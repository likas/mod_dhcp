#include "../include/moddhcp.h"

int step(struct s_state* now, struct o_packet* packet){
    printf("in step:\n");
    int ret;
    if(  parse_options(packet) == EXIT_FAILURE){ return -1; };
    print_packet(packet);

    switch( now -> state ){
        case START:
            switch ( packet -> type ){
                case DHCPDISCOVER:
                /* клиент новый */
                    *(now -> c_lease) = find_lease(packet);
                    ret = dhcpdiscover(packet, now -> c_lease -> ip);
                    if( ret == 0 ){
                        now -> state = REQUEST_WAIT;
                    }
                    break;
                case DHCPREQUEST:
                /* клиент перезагрузился, но ему известен 
                 * адрес (Т1 не истекло) */
                    
                    dhcprequest(packet, DEFAULT_DEFAULT_LEASE_TIME, \
                            now -> c_lease -> ip);
                    break;
                default:
                    now -> state = CLOSE;
                    break;
            }
            break;
        case REQUEST_WAIT:
            switch ( packet -> type ){
                case DHCPREQUEST:
                /* новый клиент выбрал сервер */
                    if( serv_addr.sin_addr.s_addr != \
                            packet -> srv_addr.s_addr ){
                        now -> state = CLOSE;
                    }else{
                        ret =  dhcprequest(packet, \
                                now -> c_lease -> time, \
                                now -> c_lease -> ip);
                        if( ret == 0 ){
                            now -> state = RENEW_WAIT;
                        }
                    }
                    break;
                case DHCPINFORM:
                /* клиент информирует о параметрах, 
                 * назначенных вручную */
//                    dhcpinform(packet);
                    break;
                default:
                    now -> state = CLOSE;
                    break;
            }
            break;
        case RENEW_WAIT:
            switch ( packet -> type ){
                case DHCPREQUEST:
                /* у клиента истекло время Т1 */
//                    dhcprequest(packet);
                    break;
                case DHCPRELEASE:
                /* клиенту больше не нужен адрес */
//                    dhcprelease(packet);
                    break;
                default:
                    now -> state = CLOSE;
                    break;
            }
            break;
        case CLOSE:
            exit(1);
            break;
        default:
            exit(1);
            break;
    }
    return EXIT_SUCCESS;
}
