#include "../include/moddhcp.h"

int parse_options(struct o_packet *packet){
	packet -> options_valid = 1;
    u_int8_t *position;
    unsigned char type;

    /* If we don't see the magic cookie, there's nothing to parse. */
	if( memcmp (packet -> raw -> options, DHCP_OPTIONS_COOKIE, 4) ) {
        printf("error in magic cookie\n");
		packet -> options_valid = 0;
		return 1;
	}
    
    /* Check if it is a BOOTREQ and type is DISCOVER/REQUEST */
    if( packet -> raw -> op == BOOTREQUEST ) {

        if( get_option( packet -> raw, DHO_DHCP_MESSAGE_TYPE, 1, UCHAR, \
                    &packet -> type ) \
                != -1 ){
            get_option(packet -> raw, DHO_DHCP_REQUESTED_ADDRESS, \
                        32, LONGINT, &(packet -> req_addr.s_addr));
            get_option(packet -> raw, DHO_DHCP_SERVER_IDENTIFIER, \
                    32, LONGINT, &(packet -> client_id));
            if( get_option(packet -> raw, DHO_END, 0, UCHAR, NULL) == 1){
                return EXIT_SUCCESS;
            }
        }
    }
    return EXIT_FAILURE;

}
