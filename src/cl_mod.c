//#include "../include/modcl.h"
#include "../include/moddhcp.h"

#define NUMBER_OF_PACKETS 2

int main(int argc, char* argv[]){
    int sock_in;
        int recv_ret,
        i;
    struct sockaddr_in addr_out;
    struct dhcp_packet *packet = malloc(sizeof( struct dhcp_packet ));
    struct o_packet *ppacket = malloc(sizeof(struct o_packet));

    srandom(time(NULL));
    /* locals */
    /* inits */


    sock_in = set_connection("eth0", &addr_out);
    int sendto_ret;
    /* end of filling packet */

    for( i = 0; i < NUMBER_OF_PACKETS; ++i ){

        if( i == 0 ){
            fill_packet(packet, DHCPDISCOVER);
        }else
        if( i == 1 ){
            fill_packet(packet, DHCPREQUEST);
        }

        sendto_ret = sendto(sock_in, packet, \
                sizeof(struct dhcp_packet), 0, \
                (struct sockaddr*) &addr_out, sizeof (struct sockaddr_in));
        if( sendto_ret == -1 ){
            perror("sendto");
            return EXIT_FAILURE;
        }else{
            printf("s_Ret = %d\n", sendto_ret);
        }

        recv_ret = recvfrom(sock_in, (void*)packet, \
                    sizeof(struct dhcp_packet), 0, NULL, 0);
        if( recv_ret == -1 ){
            perror("recvfrom");
        }
        ppacket -> raw = packet;
        parse_options(ppacket);
        print_packet(ppacket);
    }


    free(packet);
    free(ppacket);
    close(sock_in);
    return EXIT_SUCCESS;
}
