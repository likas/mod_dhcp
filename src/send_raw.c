#include "../include/moddhcp.h"

#define ETH_HDRLEN 14
#define IP4_HDRLEN 20
#define UDP_HDRLEN 8

int send_raw(int sock, struct dhcp_packet* packet, const char* iface)
{
    struct iphdr ip_hdr;
    struct udphdr udp_hdr;
    int datalen, st_ret, i;
    struct sockaddr_in my_addr;
    struct sockaddr_ll device; /* will use this as an argument of
                                * sendto() */
    struct ifreq ifr;
    uint8_t dest_mac[INET_ADDRSTRLEN];
    uint8_t src_mac[INET_ADDRSTRLEN];
    uint8_t data[IP_MAXPACKET];
    bzero(data, IP_MAXPACKET);
    bzero(&device, sizeof( struct sockaddr_ll ));
    int soa = sizeof(struct sockaddr_in );
    for( datalen = DHCP_MAX_OPTION_LEN; datalen > 0; --datalen ){
        if( packet -> options[datalen] != 0 ){
            if( packet -> options[datalen] != 255 ){
                printf("\t!! What we find, is not an END\n");
            }
            datalen = (sizeof( struct dhcp_packet) - \
                      DHCP_MAX_OPTION_LEN + \
                      datalen + 1);
            break;
        }
    }

    /* binding socket to device so we 
     * can get ip address to send from */

    if ( setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, \
                (void *)&ifr, sizeof(ifr)) < 0) {
        perror( "setsockopt binding to device failed" );
        return EXIT_FAILURE;
    }

    /* ------------ using iface to gain source mac ------------- */
    snprintf (ifr.ifr_name, sizeof (ifr.ifr_name), "%s", iface);
    if (ioctl (sock, SIOCGIFHWADDR, &ifr) < 0) {
        perror ("ioctl() failed to get source MAC address ");
        return (EXIT_FAILURE);
    }

    // Copy source MAC address.
    memcpy(src_mac, ifr.ifr_hwaddr.sa_data, 6);

    /* ----------------- filling sockaddr_ll ------------------- */
    device.sll_family = AF_PACKET;
    device.sll_protocol = htons(ETH_P_IP);
    /* длина MAC адреса */
    device.sll_halen = htons (6);
    /* индекс интерфейса */
    if ((device.sll_ifindex = if_nametoindex (iface)) == 0) {
        perror ("if_nametoindex() failed to obtain interface index ");
        exit (EXIT_FAILURE);
    }
    bzero(dest_mac, INET_ADDRSTRLEN);
    strncpy( dest_mac, (unsigned char*)(packet -> chaddr), \
        packet -> hlen );
    /* MAC-адрес */
    memcpy (device.sll_addr, src_mac, 6 * sizeof (uint8_t));
    /* --------------------- ETH_HDR --------------------------- */
    memcpy( data, packet -> chaddr, 6 * sizeof (uint8_t) );
    printf( " Destination address: \t");
    print_hwaddr( data, 6 );
    memcpy( data + 6, src_mac, 6 * sizeof (uint8_t) );
    printf(" Source address: \t");
    print_hwaddr( data + 6, 6 );
    data[12] = ETH_P_IP / 256;
    data[13] = ETH_P_IP % 256;
    /* --------------------- IP HDR ---------------------------- */
	ip_hdr.version = 4;
	ip_hdr.ihl = 5;
	ip_hdr.tos = 0;
	ip_hdr.tot_len = htons(IP4_HDRLEN + UDP_HDRLEN + datalen);
	ip_hdr.id = 0;
	ip_hdr.ttl = 65;
	ip_hdr.protocol = IPPROTO_UDP;
	ip_hdr.check = htons(0);

    //getsockname(sock, (struct sockaddr*)&my_addr, (socklen_t*)&soa);

	//ip_hdr.saddr = my_addr.sin_addr.s_addr;
    ip_hdr.saddr = inet_addr("192.168.0.100");
    ip_hdr.daddr = inet_addr("255.255.255.255");
	//ip_hdr.daddr = inet_network("127.0.0.1");
//	ip_hdr.daddr = inet_addr("127.0.0.1");

    /* ----------------------- UDP HDR ------------------------- */
    udp_hdr.source = htons(67);
    udp_hdr.dest = htons(68);
    udp_hdr.len = htons( UDP_HDRLEN + datalen);
    udp_hdr.check = 0;

    /* ----------------- filling frame ------------------------- */
    
    memcpy(data + ETH_HDRLEN, &ip_hdr, IP4_HDRLEN);
    memcpy(data + ETH_HDRLEN + IP4_HDRLEN, &udp_hdr, UDP_HDRLEN);
    memcpy(data + ETH_HDRLEN + IP4_HDRLEN + UDP_HDRLEN, \
            packet, datalen);

/*    for ( i = 0; i < IP4_HDRLEN + UDP_HDRLEN + datalen; ++i ){
        printf("%02X ", data[i]);
    } */

    /* ---------------- sending, finally ----------------------- */

    st_ret = sendto (sock, data, ETH_HDRLEN + IP4_HDRLEN + UDP_HDRLEN + \
            datalen, \
            0, (struct sockaddr *) &device, \
            sizeof(struct sockaddr_ll));
/*    st_ret = write ( sock, data, ETH_HDRLEN + IP4_HDRLEN + UDP_HDRLEN + \
            datalen); */
    if ( st_ret == -1 ) {
        perror ("sendto");
        return -1;
    }
    return st_ret;
}
