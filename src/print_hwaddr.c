#include "../include/moddhcp.h"

int print_hwaddr(unsigned char* addr, int len){
    int i;
    for( i = 0; i < len - 1; ++i){
        printf("%02X:", addr[i]);
    }
    printf("%02X\n", addr[i]);
    return EXIT_SUCCESS;
}
