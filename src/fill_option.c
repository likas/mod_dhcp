#include "../include/moddhcp.h"

int fill_option(struct dhcp_packet* packet, unsigned char option, \
        int option_type, unsigned char length, int position, ...){
    static int pointer;
    u_int32_t longint;
    va_list arg;
    if( position != -1 ){
        pointer = position + 4;
    } 
    packet -> options[pointer++] = option;
    if( length == 0 ){
        return pointer;
    }else{
        packet -> options[pointer++] = length;
        va_start(arg, position);
        if( option_type == UCHAR ){
            for( ; length != 0; length = length - 1){
                packet -> options[pointer++] = (unsigned char)va_arg(arg, \
                        unsigned int);
            }
        }else if( option_type == LONGINT ){
            longint = va_arg(arg, u_int32_t);                       
            packet -> options[pointer++] = (longint >> 24) & 0xFF;
            packet -> options[pointer++] = (longint >> 16) & 0xFF;
            packet -> options[pointer++] = (longint >> 8) & 0xFF;
            packet -> options[pointer++] = (longint) & 0xFF;
        }
        va_end(arg);
    }
/*    for( i = 0; i != pointer; ++i ){
    printf("%d ", ((int)packet -> options[i]));
    }
    printf("\n");*/
    return pointer;
}

