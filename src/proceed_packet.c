#include "../include/moddhcp.h"

int proceed_packet(struct dhcp_packet* packet, unsigned char* buffer, int size){
	unsigned short iphdr_len;
    struct iphdr *iph = (struct iphdr*)buffer;
    iphdr_len = iph -> ihl * 4; //ihl is in DWORDS (1 DWORD is 4 bytes?)
    struct udphdr *udp_hdr = (struct udphdr*)(buffer + iphdr_len);
    if( iph -> protocol == 17){
        /* check if bootp */
        if( htons(udp_hdr -> dest) == 67){
            if( htons(udp_hdr -> source) != 68 ){
                printf(" Warning: dest. port is %d\n", \
                        htons(udp_hdr -> source));
            }
            memcpy(packet, (buffer + \
                    20 + 8 ), \
                    size - (20 + 8));
            return EXIT_SUCCESS;
        }
    }
    return EXIT_FAILURE;
}
