#include "../include/moddhcp.h"

int fill_packet( struct dhcp_packet* packet, int type ){
    static int my_xid;
    int packet_size;
    srandom(time(NULL));
    bzero(packet, sizeof(struct dhcp_packet));

    packet -> op = BOOTREQUEST;
    packet -> htype = HTYPE_ETHER;
    packet -> hlen = HARDWARE_ADDR_LEN;
    packet -> hops = 0;
    packet -> secs = 0;
    packet -> flags = 0;

    inet_aton("0.0.0.0", &(packet -> ciaddr));
    inet_aton("0.0.0.0", &(packet -> yiaddr));
    inet_aton("0.0.0.0", &(packet -> siaddr));
    inet_aton("0.0.0.0", &(packet -> giaddr));
    
    strncpy(packet -> options, DHCP_OPTIONS_COOKIE, 4);
    switch( type ){
        case DHCPDISCOVER:
            my_xid = random();
            fill_option(packet, DHO_DHCP_MESSAGE_TYPE, UCHAR, \
                    1, 0, DHCPDISCOVER);
//            fill_option(

            break;
        case DHCPREQUEST:
            fill_option(packet, DHO_DHCP_MESSAGE_TYPE, UCHAR, \
                    1, 0, DHCPREQUEST);
//            fill_option(
            break;
        default:
            break;
    }
    packet -> xid = my_xid;
    packet_size = fill_option(packet, DHO_END, UCHAR, 0, -1);


    return packet_size;
}
