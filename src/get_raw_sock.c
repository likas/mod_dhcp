#include "../include/moddhcp.h"

int get_raw_sock(){
    int sock;
    sock = socket(AF_PACKET, SOCK_RAW, (htons(ETH_P_ALL)));
    if( sock == -1 ){
        perror("socket");
        exit(1);
    }
    return sock;
}
