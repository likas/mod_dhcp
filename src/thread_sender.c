#include "../include/moddhcp.h"

void* thread_sender(void* arg){
    struct o_packet ppacket;
    struct dhcp_packet* packet = malloc( sizeof( struct dhcp_packet) );
    int sock;
    char iface[IFACE_STRLEN];
    sock = get_raw_sock();
//    sock = get_dgram_sock();
    strcpy(iface, "eth2");

    while( quit ){
        bzero( packet, sizeof( struct dhcp_packet ) );
        /* getting data to send */
        get_queue(serv_send, packet, sizeof(struct dhcp_packet));
        ppacket.raw = packet;
//        print_packet(&ppacket);
        send_raw( sock, packet, iface );
//        send_cooked( sock, packet, iface );
    }

    free (packet);
    /* ends here */
    return NULL;
}
