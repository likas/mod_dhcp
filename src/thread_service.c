#include "../include/moddhcp.h"

void* thread_service(void* arg){
    int thread_id = (int)pthread_self();
    printf("%x: created\n", thread_id);
    struct queue *i_queue = (struct queue*)arg;
    struct o_packet* packet = malloc( sizeof(struct o_packet) );
    struct s_state* now = malloc( sizeof( struct s_state ) );
    now -> c_lease = malloc( sizeof( struct lease ) );
    now -> state = START;
    now -> lease_status = NONE;
    while( quit ){
        get_queue(i_queue, packet, sizeof(struct o_packet));

        if( step(now, packet) == -1 ){ continue; };

        if( now -> state == CLOSE ){
            free ( now -> c_lease );
            free ( now );
            free( packet );
            return NULL;
        }

        put_queue(serv_send, packet -> raw, sizeof(struct dhcp_packet));
    }
    free ( now -> c_lease );
    free ( now );
    free( packet );
    return NULL;
}
