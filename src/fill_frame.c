#include "../include/moddhcp.h"

#define ETH_HDRLEN 14  // Ethernet header length
#define IP4_HDRLEN 20  // IPv4 header length
#define UDP_HDRLEN  8  // UDP header length, excludes data

int fill_frame(uint8_t* eth_frame, int sock, char* mac, int mac_len, \
        struct o_packet* data, int datalen){
    struct iphdr ip_hdr;
    struct udphdr udphdr;
    int frame_length;
    struct sockaddr_in my_addr;
    socklen_t soa = sizeof (struct sockaddr_in);
    
    // ------------------------ IPv4 header --------------------------
    
	ip_hdr.version = 4;
	ip_hdr.ihl = 5;
	ip_hdr.tos = 0;
	ip_hdr.tot_len = IP4_HDRLEN + UDP_HDRLEN + datalen;
	ip_hdr.id = 0;
	ip_hdr.ttl = 65;
	ip_hdr.protocol = IPPROTO_UDP;
	ip_hdr.check = htons(0);

    getsockname(sock, (struct sockaddr*)&my_addr, &soa);

	ip_hdr.saddr = my_addr.sin_addr.s_addr;
	ip_hdr.daddr = inet_network("255.255.255.255");

    // UDP header
    // Source port number (16 bits): pick a number
    udphdr.source = htons (67);
    // Destination port number (16 bits): pick a number
    udphdr.dest = htons (68);
    // Length of UDP datagram (16 bits): UDP header + UDP data
    udphdr.len = htons (UDP_HDRLEN + datalen);
    // UDP checksum (16 bits)
    udphdr.check = 0;
//    udphdr.check = udp4_checksum (iphdr, udphdr, data, datalen);
     // Fill out ethernet frame header.
    // Ethernet frame length = ethernet header (MAC + MAC + ethernet type) + ethernet data (IP header + UDP header + UDP data)
    frame_length = IP4_HDRLEN + UDP_HDRLEN + datalen;
    // Destination and Source MAC addresses
    memcpy (eth_frame, mac, 6 * sizeof (uint8_t));
    memcpy (eth_frame + 6, mac, 6 * sizeof (uint8_t));

    // Next is ethernet type code (ETH_P_IP for IPv4).
    // http://www.iana.org/assignments/ethernet-numbers
    eth_frame[12] = ETH_P_IP / 256;
    eth_frame[13] = ETH_P_IP % 256;

    // Next is ethernet frame data (IPv4 header + UDP header + UDP data).
    // IPv4 header
    memcpy (eth_frame + ETH_HDRLEN, &ip_hdr, \
            IP4_HDRLEN * sizeof (uint8_t));
    // UDP header
    memcpy (eth_frame + ETH_HDRLEN + IP4_HDRLEN, \
            &udphdr, UDP_HDRLEN * sizeof (uint8_t));
    // UDP data
    memcpy (eth_frame + ETH_HDRLEN + IP4_HDRLEN + UDP_HDRLEN, \
            data, datalen * sizeof (uint8_t));

    print_data(eth_frame, frame_length);

    return frame_length;
}
