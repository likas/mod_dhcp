#include "../include/moddhcp.h"

int get_option(struct dhcp_packet* packet, unsigned char option, \
        int length, int option_type, void* destination){
    int pointer;
    int real_length;
    for( pointer = 4; packet -> options[pointer] != option && \
            pointer <= DHCP_MAX_OPTION_LEN; ++pointer ){}
    if( pointer == DHCP_MAX_OPTION_LEN ){
        return -1;
    }
    if( length == 0 ){
        return (option == packet -> options[pointer] ? 1 : 0);
    }else{
        real_length = packet -> options[++pointer];
        if( option_type == UCHAR ){
            memcpy( destination, &packet -> options[++pointer], length );
        }else if( option_type == LONGINT ){
            if( length == 32 ){
                memcpy( destination, \
                        &(packet -> options[++pointer]), 1 );
                memcpy( destination + 1, \
                        &(packet -> options[++pointer]), 1);
                memcpy( destination + 2, \
                        &(packet -> options[++pointer]), 1);
                memcpy( destination + 3, \
                        &(packet -> options[++pointer]), 1);
            }else if ( length == 16 ){
                memcpy( destination, \
                        &(packet -> options[++pointer]), 1 );
                memcpy( destination + 1, \
                        &(packet -> options[++pointer]), 1);
            }
        }
    }
    return real_length;
}
