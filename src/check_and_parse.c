
#include "../include/moddhcp.h"

int check_and_parse(packet)
    struct o_packet* packet;
{
//    int xid_is_missing = 0;
    if( packet -> raw -> op != BOOTREQUEST && packet -> raw -> op != BOOTREPLY ){
        printf(" op = %d (not BOOTREQUEST or BOOTREPLY!)\n", \
                packet -> raw -> op);
        return PARAMS_INCORRECT;
    }

    switch( packet -> raw -> htype ){
        case HTYPE_ETHER:
        case HTYPE_IPMP:
        case HTYPE_INFINIBAND:
        case HTYPE_FDDI:
        case HTYPE_IEEE802:
            break;
        default:
            printf("Warning: hardware address of unknown type\n");
            break;
    }

    if( packet -> raw -> xid == 0 ){
        printf("! *Warning: xid is missing\n");
//        xid_is_missing = 1;
        return PARAMS_INCORRECT;
    }
    
    if( packet -> raw -> flags != 0 && \
            packet -> raw -> flags != BOOTP_BROADCAST ){
        printf("Warning: unknown symbols in flags field\n");
    }

    if( memcmp(packet -> raw -> chaddr, "0000000000000000", 16)){
        printf("Warning: hardware address is empty\n"); 
    }

    parse_options(packet);

/*    if( packet -> options_valid == 0 ){
        return PARAMS_INCORRECT;
    }*/
    
    return EXIT_SUCCESS;

}
