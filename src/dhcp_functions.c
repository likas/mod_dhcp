#include "../include/moddhcp.h"

int dhcpdiscover(packet, lease_ip)
    struct o_packet *packet;
    struct in_addr lease_ip;
{

    /* fill parameters */
    packet -> raw -> op = BOOTREPLY;
    packet -> raw -> htype = HTYPE_ETHER;
    packet -> raw -> hlen = packet -> raw -> hlen;
    packet -> raw -> hops = 0;
    packet -> raw -> secs = 0;
    inet_aton("0.0.0.0", &(packet -> raw -> ciaddr));
//    inet_aton("0.0.0.0", &(packet -> raw -> yiaddr));
    packet -> raw -> yiaddr = lease_ip;
    inet_aton("0.0.0.0", &(packet -> raw -> siaddr));

    /* we dont touch magic cookie */
    bzero( packet -> raw -> options + 4, DHCP_MAX_OPTION_LEN - 4);
    /* options: */
    /* message type */
    fill_option( packet -> raw, DHO_DHCP_MESSAGE_TYPE, \
            UCHAR, 1, 0, DHCPOFFER );
    /* lease time(s) */
    fill_option( packet -> raw, DHO_DHCP_LEASE_TIME, \
            LONGINT, 4, -1, DEFAULT_DEFAULT_LEASE_TIME );
    /* server identifier */
    fill_option( packet -> raw, DHO_DHCP_SERVER_IDENTIFIER, LONGINT, \
            4, -1, ntohl(serv_addr.sin_addr.s_addr));
    packet -> options_length = fill_option( packet -> raw, DHO_END, UCHAR, 0, -1);
    return EXIT_SUCCESS;
}

int dhcprequest(packet, time, ip_lease)
    struct o_packet *packet;
    u_int32_t time;
    struct in_addr ip_lease;
{ 
    
    /* TODO check parameters of previously sent DHCPOFFER?  */

    /* send NAK then */
     
    
    packet -> raw -> op = BOOTREPLY;
    packet -> raw -> htype = HTYPE_ETHER;
    packet -> raw -> hlen = packet -> raw -> hlen;
    packet -> raw -> hops = 0;
    packet -> raw -> secs = 0;
    inet_aton("0.0.0.0", &(packet -> raw -> ciaddr));
    packet -> raw -> yiaddr = ip_lease ;
    
    bzero(packet -> raw -> options + 4, DHCP_MAX_OPTION_LEN - 4);

    fill_option(packet -> raw, DHO_DHCP_LEASE_TIME, LONGINT, 4, 0, \
            time);
    fill_option(packet -> raw, DHO_DHCP_MESSAGE_TYPE, UCHAR, 1, -1, \
            DHCPACK);
    fill_option(packet -> raw, DHO_DHCP_SERVER_IDENTIFIER, \
            LONGINT, 4, -1, ntohl(serv_addr.sin_addr.s_addr));

    packet -> options_length = fill_option(packet -> raw, DHO_END, UCHAR, 0, -1);

    return EXIT_SUCCESS;
}
