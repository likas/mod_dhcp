#include "../include/moddhcp.h"

int pid_action(char* argu){
    int pid_file;
    pid_t pid;
    char pid_buffer[16];
        if( argu == NULL ){    
        pid = getpid();
        pid_file = creat("pid_file", 0);
        if( pid_file == -1 ){
            perror("creat");
            return EXIT_FAILURE;
        }
        write(pid_file, (void*)&pid, sizeof(pid));
        close(pid_file);
    }else{
        if( ( strcmp(argu, "stop") ) == 0 ){
            pid_file = open("pid_file", O_RDONLY, 0);
            read( pid_file, pid_buffer, 16);
            pid = atoi( pid_buffer );
                        
            kill(pid, SIGINT);
            if ( remove("pid_file") == -1 ){
                perror("remove");
            }
            return EXIT_SUCCESS;
        }
    }
    return EXIT_SUCCESS;
}
